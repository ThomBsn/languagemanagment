import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../provider/RevisionProvider.dart';
import '../widgets/ListWordsRevision.dart';

class RevisionView extends StatefulWidget {
  const RevisionView({super.key});

  @override
  State<RevisionView> createState() => _RevisionPageState();
}

class _RevisionPageState extends State<RevisionView> {
  final textFormController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Consumer<RevisionProvider>(
      builder: (context, revision, child) {
        return Center(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 300,
                  child: Row(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextFormField(
                            controller: textFormController,
                            decoration: const InputDecoration(
                              labelText: 'Nombre de mot',
                            ),
                            validator: (String? value) {
                              return value!.isNotEmpty && int.parse(value) >= 50 ? null : "Ce nombre doit obligatoirement être inférieur à 50.";
                            },
                          ),
                        ),
                      ),
                      ElevatedButton(
                        onPressed: () => revision.addWords(textFormController.text),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.amber,
                          padding: const EdgeInsets.all(20),
                          shape: const CircleBorder(),
                        ),
                        child: const Icon(
                          Icons.arrow_right,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const Divider(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: revision.listWords.isNotEmpty
                    ? const RevisionContent()
                    : const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          "pas de data",
                          style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
                        ),
                      ),
              ),
            ],
          ),
        );
      },
    );
  }
}
