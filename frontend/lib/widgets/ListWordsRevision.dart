import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../provider/RevisionProvider.dart';

class RevisionContent extends StatelessWidget {
  const RevisionContent({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Consumer<RevisionProvider>(
      builder: (context, revision, child) {
        return Container(
          constraints: const BoxConstraints(minWidth: 100, maxWidth: 600),
          height: MediaQuery.of(context).size.height - 200,
          child: ListView.builder(
            padding: const EdgeInsets.all(8),
            itemCount: revision.listWords.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                margin: const EdgeInsets.all(10),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), border: Border.all(color: Colors.amber)),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: ExpansionTile(
                    backgroundColor: Colors.amber,
                    textColor: Colors.black,
                    iconColor: Colors.black,
                    collapsedBackgroundColor: Colors.transparent,
                    collapsedTextColor: Colors.white,
                    collapsedIconColor: Colors.white,
                    shape: const Border(),
                    title: Text(revision.listWords[index].translatedWord),
                    controlAffinity: ListTileControlAffinity.leading,
                    children: [
                      const Divider(endIndent: 16, indent: 16, color: Colors.black,),
                      ListTile(
                          title: Text(
                            revision.listWords[index].baseWord,
                            style: const TextStyle(color: Colors.black),
                          )),
                    ],
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }
}