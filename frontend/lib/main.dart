import 'package:flutter/material.dart';
import 'package:frontend/provider/RevisionProvider.dart';
import 'package:provider/provider.dart';
import 'router/index.dart';

void main() =>  runApp(MultiProvider(
  providers: [
    ChangeNotifierProvider(create: (context) => RevisionProvider()),
  ],
  child: const MyApp(),
));

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Plura',
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),
      routerConfig: router,
      debugShowCheckedModeBanner: false,
    );
  }
}
