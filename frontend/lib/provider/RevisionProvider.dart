import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:frontend/api/revisionApi.dart';

import '../classes/models/WordModel.dart';

class RevisionProvider extends ChangeNotifier {

  final List<Word> _listWord = [];

  List<Word> get listWords => _listWord;

  void addWords(String limitWords) async {
    // Await the http get response, then decode the json-formatted response.
    await revisionApi.getCollection(1, 2, limitWords).then((Response response) {
      emptyList();
      for (Word element in response.data) {
        _listWord.add(element);
      }
    }).catchError((error) {
      if (kDebugMode) {
        print('Request failed with status: $error.');
      }
    });

    notifyListeners();
  }

// void removeWord(Pizza item) {
//   if (_list.keys.contains(item)) {
//     _list[item]! > 1 ? _list[item] = _list[item]! - 1 : _list.remove(item);
//     _total -= item.price;
//   }
//   notifyListeners();
// }

  void emptyList() {
    _listWord.clear();
    notifyListeners();
  }
}


