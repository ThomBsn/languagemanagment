import '../interfaces/entity_interface.dart';

class Word implements EntityInterface {
  Word({
    required this.baseWord,
    required this.translatedWord,
    this.baseLanguageId,
    this.translationLanguageId,
  });

  @override
  factory Word.fromMap(Map map) {
    return Word(baseWord: map['baseWord'], translatedWord: map['translatedWord']);
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'baseWord': baseWord,
      'translatedWord': translatedWord,
      'baseLanguageId': baseLanguageId,
      'translationLanguageId': translationLanguageId,
    };
  }

  late int id;
  late String baseWord;
  late String translatedWord;
  late int? baseLanguageId;
  late int? translationLanguageId;
}
