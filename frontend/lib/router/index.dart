import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../views/RevisionView.dart';


final _rootNavigatorKey = GlobalKey<NavigatorState>();
final _shellNavigatorKey = GlobalKey<NavigatorState>();

// Router configuration
final router = GoRouter(
  navigatorKey: _rootNavigatorKey,
  routes: [
    ShellRoute(
      navigatorKey: _shellNavigatorKey,
      pageBuilder: (context, state, child) {
        return NoTransitionPage(
          child: Scaffold(
            appBar: AppBar(
              toolbarHeight: 70,
              backgroundColor: Colors.amber,
              title: const Text(
                'Plura',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 25,
                  fontWeight: FontWeight.bold
                ),
              ),
              actions: const <Widget>[
                // TextButton(
                //   onPressed: () => {print("vocab")},
                //   child: const Text("Vocabulaire"),
                // ),
                // TextButton(
                //   onPressed: () => {print("Conjugaison")},
                //   child: const Text("Conjugaison"),
                // ),
                // TextButton(
                //   onPressed: () => {print("Révision")},
                //   child: const Text("Révision"),
                // ),
              ]
            ),
            body: SafeArea(
              child: child,
            ),
          ),
        );
      },
      routes: [
        GoRoute(
          name: "revision",
          path: '/',
          pageBuilder: (context, state) => const NoTransitionPage(
            child: RevisionView(),
          ),
        ),
      ],
    ),
  ],
);
