import 'package:dio/dio.dart';
import 'package:frontend/classes/models/WordModel.dart';
import 'api.dart';


class revisionApi {
  static String url = "/words";

  static Future<Response> getCollection(int baseLanguageId, int translationLanguageId, String limitWords) async {
    Response response = await api.get(url, queryParameters: {'baseLanguageId': baseLanguageId, 'translationLanguageId': translationLanguageId, 'limit': limitWords});

    //Transforme le json en model word
    _jsonToDto(response);

    return response;
  }

  static void _jsonToDto(Response<dynamic> response) {
    List<Word> collection = [];
    for (Map<String, dynamic> entity in response.data) {
      collection.add(Word.fromMap(entity));
    }
    response.data = collection;
  }
}