# Plura

This project will be a place for new learners to enjoy new languages.

## Features

The following list contains all of the features in the app. 
- Flash cards
- Personal vocab list
- Import your vocab
- A score system to add fun in your learning exercises