const express = require("express")
const mysql = require('mysql2');
const bodyParser = require('body-parser');
const cors = require('cors');

// // MySQL Connection
const pool = mysql.createPool({
    host: 'mysql-tbsn.alwaysdata.net',
    user: 'tbsn',
    password: 'alwaysthom_45',
    database: 'tbsn_languages',
    waitForConnections: true,
});

const api = express()

// Middleware
api.use(bodyParser.urlencoded({ extended: true }));
api.use(bodyParser.json());
api.use(cors({
    origin: "*"
}))

// Create a new language
api.post('/new/language', (req, res) => {
    const { name } = req.body;
    const query = `INSERT INTO languages (language) VALUES ${name}`;
    pool.query(query, (err, results) => {
        if (err) {
            console.error(err);
            return res.status(500).json({ error: 'Internal Server Error' });
        }
        return res.status(200).json(results);
    });
});

api.get('/', async (req, res) => {
    return res.status(200).json("coucou");
});

// Get all languages
api.get('/languages', async (req, res) => {
    pool.query(`SELECT * FROM languages`, (err, results) => {
        if (err) {
            console.error(err);
            return res.status(500).json({ error: 'Internal Server Error' });
        }
        return res.status(200).json(results);
    });
});

// Create a new word
api.post('/new/words', (req, res) => {
    const { languageId, frenchWord, translatedWord } = req.body;
    const query = `INSERT INTO translations (languageId, frenchWord, translatedWord) VALUES (${languageId}, ${frenchWord}, ${translatedWord})`;
    pool.query(query, (err, results) => {
        if (err) {
            console.error(err);
            return res.status(500).json({ error: 'Internal Server Error' });
        }
        return res.status(200).json(results);
    });
});

// Get all words
api.get('/words', async (req, res) => {
    const { languageId } = req.body;
    pool.query("SELECT frenchWord, translatedWord FROM translations WHERE languageId = ?", [languageId], (err, results) => {
        if (err) {
            console.error(err);
            return res.status(500).json({ error: 'Internal Server Error' });
        }
        return res.status(200).json(results);
    });
});
const port = process.env.PORT || 3000 

api.listen(port, () => console.log(`Listening to port ${port}`));